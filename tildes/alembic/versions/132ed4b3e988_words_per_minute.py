"""Words per Minute

Revision ID: 132ed4b3e988
Revises: 55f4c1f951d5
Create Date: 2023-06-07 23:44:32.533936

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "132ed4b3e988"
down_revision = "55f4c1f951d5"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("users", sa.Column("words_per_minute", sa.Integer(), nullable=True))


def downgrade():
    op.drop_column("users", "words_per_minute")
